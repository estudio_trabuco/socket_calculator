
class Calculator:
    def __init__(self, client):
        self.client = client

    def calc(self, operation):
        self.client.connect()
        self.client.send(operation)
        response = self.client.getText()
        self.client.close()
        result = Result(response)
        return result.get_content()

class Result:
    ok = 'OK'
    error = 'ERROR'
    separator = ':'

    def __init__(self, response_text):
        self.response = response_text
        self.splited_response = self.response.split(self.separator)

    def has_error(self):
        if not self.splited_response[0] == self.ok:
            return True
        return False

    def get_content(self):
        return self.separator.join(
            self.splited_response[1:]
        )
