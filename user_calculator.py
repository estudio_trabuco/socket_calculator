from calculator import Calculator
from client_jorobany import ClientJorobany

host = 'mob.bit4bit.in'
# port = 65519
# string_control = '\n'
port = 65520
string_control = '\n\n'

client = ClientJorobany(host, port, string_control)
calculator = Calculator(client)

input_message = "Ingrese operacion (q para salir): "

user_input = input(input_message)
while not user_input == 'q':
    operation = user_input
    print(calculator.calc(operation))
    user_input = input(input_message)
