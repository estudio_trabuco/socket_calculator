from calculator import Calculator
from client_jorobany import ClientJorobany

host = 'localhost'
port = 65517

operation = '2+3'
expected_result = '5\n'

client = ClientJorobany(host, port)
calculator = Calculator(client)
result = calculator.calc(operation)
assert result == expected_result
