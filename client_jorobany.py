import socket

class ClientJorobany:
    coding = 'utf-8'

    def __init__(self, host, port, string_control='\n'):
        self.host = host
        self.port = port
        self.string_control = string_control

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def getText(self):
        string_control_idx = 0
        complete_text = ''
        tmp_string_control = ''
        while True:
            char = self.socket.recv(1).decode(self.coding)
            if char == self.string_control[string_control_idx]:
                tmp_string_control += char
                if tmp_string_control == self.string_control:
                    break
                string_control_idx += 1
                continue
            else:
                complete_text += tmp_string_control + char
                tmp_string_control = ''
                string_control_idx = 0
        return complete_text

    def send(self, data):
        fixed_data = data + self.string_control
        self.socket.sendall(fixed_data.encode())

    def close(self):
        self.socket.close()
