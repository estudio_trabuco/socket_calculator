import socket

class ClientJorobany:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def getText(self, buffersize):
        binaryText = self.socket.recv(buffersize)
        if binaryText:
            return binaryText.decode('utf-8')
        return ''

    def send(self, data):
        self.socket.sendall(data)

    def close(self):
        self.socket.close()

# c = ClientJorobany('mob.bit4bit.in', 65303)
# c.connect()
# print(c.getText(512))
# c.close()
